%%%%%%%%%%%%%%%% field maps

RE_file='../3D_RF/3D_RE.fld.gz'; % in V/m
IE_file='../3D_RF/3D_IE.fld.gz'; % in V/m
RH_file='../3D_RF/3D_RH.fld.gz'; % in A/m
IH_file='../3D_RF/3D_IH.fld.gz'; % in A/m

% Output_file='field_SW_half.dat.gz';

%%%%%%%%%%%%%%%% global variables

global Structure

%%%%%%%%%%%%%%%% bunch parameters

Structure.frequency = 2998.8e6; % Hz
Structure.direction = +1.0; % 
Structure.P_map = 15e6; % W design input power
Structure.P_actual = 15e6; % W actual input power

%%%%%%%%%%%%%%%% some constants

mu0 = 1.2566371e-06; % 4 pi 1e-7 H/m
clight = 299792458; % m/s

%%%%%%%%%%%%%%%% load files and eliminate Nan's

RE = load(RE_file);
IE = load(IE_file);
RH = load(RH_file);
IH = load(IH_file);

%%%%%%%%%%%%%%%%

hx = hy = hz = 1; % mm

RE(:,1) = hx*round(1e3*RE(:,1)/hx); % mm
RE(:,2) = hy*round(1e3*RE(:,2)/hy); % mm
RE(:,3) = hz*round(1e3*RE(:,3)/hz); % mm

Structure.xa = unique(RE(:,1)); % mm
Structure.ya = unique(RE(:,2)); % mm
Structure.za = unique(RE(:,3)); % mm

Nx=length(Structure.xa);
Ny=length(Structure.ya);
Nz=length(Structure.za);
Structure.Nx=Nx;
Structure.Ny=Ny;
Structure.Nz=Nz;

Structure.Ex = zeros(Nx,Ny,Nz);
Structure.Ey = zeros(Nx,Ny,Nz);
Structure.Ez = zeros(Nx,Ny,Nz);
Structure.Bx = zeros(Nx,Ny,Nz);
Structure.By = zeros(Nx,Ny,Nz);
Structure.Bz = zeros(Nx,Ny,Nz);

%%%%%%%%%%%%%%%% prepare the output field maps

for n=1:rows(RE)
    i = find(Structure.xa==RE(n,1));
    j = find(Structure.ya==RE(n,2));
    k = find(Structure.za==RE(n,3));
    Structure.Ex(i,j,k) = complex(RE(n,4),IE(n,4)); % V/m
    Structure.Ey(i,j,k) = complex(RE(n,5),IE(n,5)); % V/m
    Structure.Ez(i,j,k) = complex(RE(n,6),IE(n,6)); % V/m
    Structure.Bx(i,j,k) = mu0 * complex(RH(n,4),IH(n,4)); % T
    Structure.By(i,j,k) = mu0 * complex(RH(n,5),IH(n,5)); % T
    Structure.Bz(i,j,k) = mu0 * complex(RH(n,6),IH(n,6)); % T
end

%%%%%%%%%%%%%%%% 

Structure.za -= Structure.za(1);

Structure.xa /= 1e3; % m
Structure.ya /= 1e3; % m
Structure.za /= 1e3; % m

% save('-binary', '-zip', Output_file, 'Structure')

%%%%%%%%%%%%%%%% Mirror in Z

Output_file='field_SW_full.dat.gz';

za = -flip(Structure.za(2:end));

Ex = -flipdim(Structure.Ex(:,:,2:end), 3);
Ey = -flipdim(Structure.Ey(:,:,2:end), 3);
Ez =  flipdim(Structure.Ez(:,:,2:end), 3);
Bx =  flipdim(Structure.Bx(:,:,2:end), 3);
By =  flipdim(Structure.By(:,:,2:end), 3);
Bz = -flipdim(Structure.Bz(:,:,2:end), 3);

Structure.za = cat(1, za, Structure.za);
Structure.za -= Structure.za(1);

Structure.Ex = cat(3, Ex, Structure.Ex);
Structure.Ey = cat(3, Ey, Structure.Ey);
Structure.Ez = cat(3, Ez, Structure.Ez);
Structure.Bx = cat(3, Bx, Structure.Bx);
Structure.By = cat(3, By, Structure.By);
Structure.Bz = cat(3, Bz, Structure.Bz);

save('-binary', '-zip', Output_file, 'Structure')





